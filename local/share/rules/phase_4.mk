# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

# gff3 from previous maker-p run
extern ../phase_2/final_maker.gff.gz as MAKER_GFF3
extern ../phase_2/all.maker.transcripts.fasta.gz as MAKER_TRANS
extern ../phase_2/all.maker.proteins.fasta.gz as MAKER_PROT


log:
	mkdir -p $@

tmp:
	mkdir -p $@

ALN = $(addsuffix .tophat2.bam,$(SAMPLES))
%.tophat2.bam:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_1/$(SAMPLE)/accepted_hits_RG.bam $(SAMPLE).tophat2.bam) \
	) \
	sleep 3


# calculates statistics about flags 
FSTATS = $(addsuffix .tophat2.fstats,$(SAMPLES))
%.tophat2.fstats: %.bam
	$(call load_modules); \
	samtools flagstat $< \
	| bawk '!/^[\#+,$$]/ { \
	split($$0,a,"+ 0 "); \
	gsub(" ","",a[1]); \
	print a[1]; }' \
	| transpose \
	| sed 's/^/$*\t/'  >$@


# # for testing purpose
# query.fasta:
	# error_ignore \
	# cat <$(QUERY) \
	# | fasta2tab \
	# | head -n 1 \
	# | tab2fasta 2 >$@

query.fasta:
	ln -sf $(QUERY) $@

proteins.fasta.gz:
	ln -sf $(MAKER_PROT) $@

transcripts.fasta.gz:
	ln -sf $(MAKER_TRANS) $@

# select only final annotations from maker
annot.gff3:
	$(call load_modules); \
	h=$$(error_ignore zcat $(MAKER_GFF3) | bawk '{ if ( $$1 !~ /^\#+/ ) { exit 0 } print $$0; }'); \
	zcat $(MAKER_GFF3) \
	| bawk '!/^[$$,\#]/ { if ( $$2 ~ /^maker$$/ ) print $$0 }' \
	| cat <(echo "$$h") - \
	| gt gff3 -addids no -sort -o $@

# generate mapping file for renaming of gene names
annot.map: annot.gff3
	$(call load_modules);
	export PATH=/iga/scripts/dev_modules/src/sw/bio/maker/maker-2.31.8/bin:$$PATH;
	str="$(call first,$(SAMPLES))";
	#str1="PN40024";
	str1="$${str%-*}";
	maker_map_ids --prefix "$${str1^^}_" --justify 6 $< >$@

# rename gene names
id.annot.gff3.gz: annot.map annot.gff3
	$(call load_modules);
	export PATH=/iga/scripts/dev_modules/src/sw/bio/maker/maker-2.31.8/bin:$$PATH;
	map_gff_ids $< $^2 \
	&& sed 's/;$$//' <$^2 \   * the script map_gff_ids adds illegal ";" at the end of each line that crashes gff2bed and bedtools. Here I remove it. *
	| gzip -c9 >$@

# rename transcripts and proteins
id.%.fasta.gz: annot.gff3 %.fasta.gz annot.map 
	$(call load_modules);
	export PATH=/iga/scripts/dev_modules/src/sw/bio/maker/maker-2.31.8/bin:$$PATH;
	TMP=$$(mktemp XXXX.fasta);
	bawk '!/^[$$,\#]/ { \   * prefix the fasta ID with the mRNA ID from the gff *
	if ( $$9 ~ /^ID=mRNA/ ) { \
		split($$9,token,";"); \
		split(token[1],mrna,"="); \
		split(token[3],name,"="); \
		print mrna[2], name[2]; \
	} \
	}' $< \
	| translate -a \
	<(zcat <$^2 \
	| sed '/^$$/d' \
	| fasta2tab \
	| tr -s ' ' \\t) 2 \
	| awk -F '\t' '{ print $$1"\t"$$NF }' \   * retain only the mRNA ID and the sequence *
	| bsort \
	| tab2fasta 2 >$$TMP;
	map_fasta_ids $^3 $$TMP \   * map the new id *
	&& gzip -c9 <$$TMP >$@ \
	&& rm $$TMP





.META: %.exon.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered

SCAFF_GENE_COV = $(addsuffix .gene.cov,$(SAMPLES))
id.%.gene.cov: %.tophat2.bam id.annot.gff3.gz
	module purge;
	module load compilers/gcc/4.8.2;
	module load tools/bedops/2.4.2;
	module load tools/bedtools/2.20.1;
	if [ -e "$^2" ]; then \
		bedtools coverage \
		-abam $< \
		-b <(zcat $^2 | gff2bed | sortBed -i stdin) \
		>$@
	else
		touch $@
	fi


.PRECIOUS: merged.tophat2.bam
merged.tophat2.bam: log tmp $(ALN)
	$(call load_modules); \
	java -Xmx4g \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar MergeSamFiles \
	TMP_DIR=$^2 \
	$(addprefix INPUT=,$(ALN)) \
	OUTPUT=$@ \
	USE_THREADING=True \
	MERGE_SEQUENCE_DICTIONARIES=True \
	VALIDATION_STRINGENCY=LENIENT \
	COMPRESSION_LEVEL=9 \   * maximum compression level *
	3>&1 1>&2 2>&3 \
	| tee $</picard-MergeSamFiles.$@.log



merged.tophat2.fstats: merged.tophat2.bam
	$(call load_modules); \
	samtools flagstat $< \
	| bawk '!/^[\#+,$$]/ { \
	split($$0,a,"+ 0 "); \
	gsub(" ","",a[1]); \
	print a[1]; }' \
	| transpose \
	| sed 's/^/$@\t/'  >$@


total.gene.cov: merged.tophat2.bam id.annot.gff3.gz
	module purge;
	module load compilers/gcc/4.8.2;
	module load tools/bedops/2.4.2;
	module load tools/bedtools/2.20.1;
	module load tools/samtools/1.3;
	if [ -e "$^2" ]; then \
		bedtools coverage \
		-abam $< \
		-b <(zcat $(call last,$^) | gff2bed | sortBed -i stdin) \
		>$@
	else
		touch $@
	fi


# CALCULATE STATISTICA FOR ANNOTATIONS
id.annot.gff3.stat: id.annot.gff3.gz
	$(call load_modules); \
	gt gff3 -tidy -sort -addintrons $< \
	2>/dev/null \
	| gt stat -o $@ \
	-genelengthdistri \
	-exonlengthdistri \
	-exonnumberdistri \
	-intronlengthdistri \
	-cdslengthdistri


# temporary files
gene_length_dist.txt: id.annot.gff3.stat
	bawk '/gene length distribution/{flag=1;next}/exon length distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

exon_length_dist.txt: id.annot.gff3.stat
	bawk '/exon length distribution/{flag=1;next}/exon number distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

exon_number_distr.txt: id.annot.gff3.stat
	bawk '/exon number distribution/{flag=1;next}/intron length distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

intron_length_dist.txt: id.annot.gff3.stat
	bawk '/intron length distribution/{flag=1;next}/CDS length distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

CDS_length_distr.txt: id.annot.gff3.stat
	bawk '/CDS length distribution/{flag=1;next}/$^/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@


# plots
id.annot.gff3.stat.jpg: gene_length_dist.txt exon_length_dist.txt exon_number_distr.txt intron_length_dist.txt CDS_length_distr.txt
	cat <<'EOF' | bRscript -
	
	options(width=10000)
	setwd("$(PWD)")
	
	#pdf("$@", onefile=T)
	jpeg("$@", width=6.61, height=10, res=1600, units='in', type="cairo")
	
	gene_length = bread(file="$<", header=F)
	exon_length = bread(file="$^2", header=F)
	exon_number = bread(file="$^3", header=F)
	intron_length = bread(file="$^4", header=F)
	cds_length = bread(file="$^5", header=F)
	
	
	par(mfrow=c(3,2)) # Divide the graphics window into N x M matrix
	
	gene_length_num = rep( gene_length[[1]], gene_length[[2]] ) # repeat each element in fisrt vector the number of time in the second vector
	bins = c( seq(from=1, to=max(gene_length[[1]]), by=400), max(gene_length[[1]]) )
	
	
	suppressWarnings(hist(	gene_length_num,
							main="Gene length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,4500),
							xlim=c(0,20000),
							freq=T,
							cex.axis=0.7
						))
	
	mtext("$(call first,$(SAMPLES))", side=3)
	
	
	#####
	exon_length_num = rep( exon_length[[1]], exon_length[[2]] )
	bins = c( seq(from=1, to=max(exon_length[[1]]), by=50), max(exon_length[[1]]) )
	
	suppressWarnings(hist(	exon_length_num,
							main="Exon length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,85000),
							xlim=c(0,2000),
							freq=T,
							cex.axis=0.7
						))
	
	####
	exon_number_num = rep( exon_number[[1]], exon_number[[2]] )
	bins = c( seq(from=1, to=max(exon_number[[1]]), by=1) )
	
	suppressWarnings(hist(	exon_number_num,
							main="Exon number ditribution",
							xlab="exon number #",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,15000),
							xlim=c(0,30),
							freq=T,
							cex.axis=0.7
						))
	
	####	
	intron_length_num = rep( intron_length[[1]], intron_length[[2]] ) # repeat each element in fisrt vector the number of time in the second vector
	bins = c( seq(from=1, to=max(intron_length[[1]]), by=100), max(intron_length[[1]]) )
	
	suppressWarnings(hist(	intron_length_num,
							main="Intron length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,60000),
							xlim=c(0,5000),
							freq=T,
							cex.axis=0.7
						))
						
	####	
	cds_length_num = rep( cds_length[[1]], cds_length[[2]] ) # repeat each element in fisrt vector the number of time in the second vector
	bins = c( seq(from=1, to=max(cds_length[[1]]), by=100), max(cds_length[[1]]) )
	
	suppressWarnings(hist(	cds_length_num,
							main="CDS length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,3500),
							xlim=c(0,5000),
							freq=T,
							cex.axis=0.7
						))		
	dev.off()
	
	stats = list(	"gene_length"=gene_length_num, 
					"exon_length"=exon_length_num,
					"exon_number"=exon_number_num,
					"intron_length"=intron_length_num,
					"cds_length"=cds_length_num
				)
	
	# add total lenght to matrix returned by sapply
	with_len = rbind( 	sapply(stats, summary), c( 	sum(gene_length_num),
													sum(exon_length_num),
													sum(exon_number_num),
													sum(intron_length_num),
													sum(cds_length_num)     
												  )  
					)
	row.names(with_len)[7] = c("Tot.")
	
	# get some basic statistics
	bwrite( with_len, file="$(basename $@).summary", row.names=T, col.names=T )
	EOF




# from previous rule
id.annot.gff3.stat.summary: id.annot.gff3.stat.jpg
	touch $@




.PHONY: test
test:
	


ALL += total.gene.cov \
	id.annot.gff3.stat.jpg \
	id.annot.gff3.stat.summary \
	id.annot.gff3.gz \
	id.transcripts.fasta.gz \
	id.proteins.fasta.gz \
	merged.tophat2.fstats
	#$(SCAFF_EXON_COV) \
	#annot.gff3.gz

INTERMEDIATE += CDS_length_distr.txt \
	exon_length_dist.txt \
	exon_number_distr.txt \
	gene_length_dist.txt \
	intron_length_dist.txt \
	id.annot.gff3

CLEAN +=
