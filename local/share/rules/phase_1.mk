# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>


logs:
	mkdir -p $@

default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/$@

# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
RG_FASTQ_GZ = $(addsuffix .RG,$(SAMPLES))
%.1.fastq.gz  %.2.fastq.gz  %.RG:
		@echo installing links ... \
		$(foreach SAMPLE,$(SAMPLES), \
				$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
				$(shell ln -sf $(call get,$(SAMPLE),2) $(SAMPLE).2.fastq.gz) \
				$(shell echo -E '$(call get,$(SAMPLE),RG)' | sed -e 's/\bRG..\b/\L&/g' >$(SAMPLE).RG) \   * make RG.. to lowercase  to pass them to pipeline *
		) \
		&& sleep 3



LAUNCH = $(addsuffix .launch,$(SAMPLES))
TRANSCRIPTS_GTF = $(addsuffix /cufflinks_denovo/transcripts.gtf,$(SAMPLES))

# launch pipeline  for:
# 1. align to the reference with tophat2
# 2. add read groups with picard
# 3. assemble transcripts with cufflinks_denovo without gtf guidance
# 4. calculate some statistics
.PHONY: %.launch
%.launch: default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp %.1.fastq.gz %.2.fastq.gz %.RG
	!threads
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--skip-steps=s3,s8 \   * skip cufflinks_strict, cuffquant *
	--label "$@.$(basename $<)" \
	--templates \
	library_type=$(call get,$*,ST) \
	output_dir="$$PWD" \
	output_prefix="$(basename $@)" \
	reference_sequence="$(QUERY)" \
	read_1=$^2 \
	read_2=$^3 \
	threads=$$THREADNUM \
	$(shell cat $^4)

# merge gene annotation from different rnaseq libraries into
# single annotation
cuffmerge/merged.gtf: logs
	!threads
	$(call load_modules); \
	cuffmerge \
	-o cuffmerge \
	-p "$$THREADNUM" \
	-s $(QUERY) \
	<(echo "$(addprefix $(PWD)/,$(TRANSCRIPTS_GTF))" | tr ' ' '\n') \   * generate transcripts list *
	2>&1 \
	>$</$(notdir $@).log

# convert cuffmerge gtf to gff3 for maker
merged.gff3: logs cuffmerge/merged.gtf
	$(call load_modules); \
	export PATH=/iga/scripts/dev_modules/src/sw/bio/maker/maker-2.31.8/bin:$PATH; \
	cufflinks2gff3 $^2 >$@ \
	2>$</$@.log

# merge all alignments plus read froups into a single alignment
merged.accepted_hits_RG.bam: logs tmp
	$(load_modules); \
	java -Xmx4g \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar MergeSamFiles \
	TMP_DIR=$^2 \
	$(addprefix INPUT=./,$(addsuffix /accepted_hits_RG.bam,$(SAMPLES))) \
	OUTPUT=$@ \
	USE_THREADING=True \
	MERGE_SEQUENCE_DICTIONARIES=True \
	VALIDATION_STRINGENCY=LENIENT \
	COMPRESSION_LEVEL=9 \   * maximum compression level *
	3>&1 1>&2 2>&3 \
	| tee $</picard-MergeSamFiles.$@.log

# compute coverage in wig format
merged.accepted_hits_RG.wig: logs merged.accepted_hits_RG.bam
	module purge; \
	module load sw/bio/ngstools/1.0; \
	nt-compute-profile \
	--fasta $(QUERY) \
	--input-file $^2 \
	--output-file $@ \
	3>&1 1>&2 2>&3 \
	| tee $</nt-compute-profile.$@.log

# generate size for each
# sequence of the query
ref.size:
	$(load_modules); \
	fasta_length <$(QUERY) >$@


# tranform to compressed bigWig
merged.accepted_hits_RG.bw: merged.accepted_hits_RG.wig ref.size
	module purge; \
	module load sw/bio/ucsc-utils/default; \
	wigToBigWig $< $^2 $@


.PHONY: test
test:
	@echo

ALL += default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(RG_FASTQ_GZ) \
	$(LAUNCH)

INTERMEDIATE +=

CLEAN += 
