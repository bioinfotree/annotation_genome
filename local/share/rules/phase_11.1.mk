# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

context prj/line_date2


# self.trans.fasta: self.all.fasta pn_ref.gene.del.ref.bed
	# $(call load_modules); \
	# fasta2tab <$< \
	# | filter_1col 1 <(bawk '!/^[\#+,$$]/ { if ( $$8 == "gene" && $$20 == "0" ) { print $$4"-RA" } }' $^2) \
	# | bsort \
	# | tab2fasta 2 >$@

# other.trans.fasta: other.all.fasta rk_4.gene.del.ref.bed
	# $(call load_modules); \
	# fasta2tab <$< \
	# | filter_1col 1 <(bawk '!/^[\#+,$$]/ { if ( $$8 == "gene" && $$20 == "0" ) { print $$4"-RA" } }' $^2) \
	# | bsort \
	# | tab2fasta 2 >$@


# make blast db
self.trans.fasta.nin: self.trans.fasta
	$(call load_modules); \
	makeblastdb -in $< -dbtype nucl -title $(basename $<)

# make blast db
other.trans.fasta.nin: other.trans.fasta
	$(call load_modules); \
	makeblastdb -in $< -dbtype nucl -title $(basename $<)


# blastn
self.trans.fasta.asn.1: self.trans.fasta other.trans.fasta.nin
	!threads
	$(call load_modules); \
	blastn -evalue $(EVAL_FILTER) -num_threads $$THREADNUM -outfmt 11 -max_target_seqs 4 -query $< -db $(basename $^2) -out $@

# blastn
other.trans.fasta.asn.1: other.trans.fasta self.trans.fasta.nin
	!threads
	$(call load_modules); \
	blastn -evalue $(EVAL_FILTER) -num_threads $$THREADNUM -outfmt 11 -max_target_seqs 4 -query $< -db $(basename $^2) -out $@



.META: self.trans.fasta.tab other.trans.fasta.tab self.trans.best.fasta.tab other.trans.fasta.tab
	1	qacc	Query accesion
	2	qlen	Query sequence length
	3	sacc	Subject accession
	4	slen	Subject sequence length
	5	evalue	Expect value
	6	qstart	Start of alignment in query
	7	qend	End of alignment in query
	8	sstart	Start of alignment in subject
	9	send	End of alignment in subject
	10	length	Alignment length
	11	sseq	Aligned part of subject sequence
	12	sstrand	Subject strand
	13	pident	Perc. of identity

self.trans.fasta.tab: self.trans.fasta.asn.1
	$(call load_modules); \
	blast_formatter -archive $< -outfmt "6 qacc qlen sacc slen evalue qstart qend sstart send length sseq sstrand pident" -out $@

other.trans.fasta.tab: other.trans.fasta.asn.1
	$(call load_modules); \
	blast_formatter -archive $< -outfmt "6 qacc qlen sacc slen evalue qstart qend sstart send length sseq sstrand pident" -out $@



# parse blast results in tab format
# end return the best match of the query
# making sure that is not the selfmatch
define get_best
	bsort -k 1,1 -k 13,13nr $1 \
	| bawk 'BEGIN { i=0; start=""; } !/^[\#,$$]/ { \   * sort for best identity % *
	if ( $$1 != $$3 && $$13 >= 90 ) { \   * select for minimum identity *
		if ( NR == 1 ) { \
			start=$$1; \
			print $$0; \
			} \
		else { \
			if ( $$1 != start ) { \
				start=$$1; \
				print $$0; \
				} \
			} \
		} \
	}' >$2
endef

self.trans.fasta.best.tab: self.trans.fasta.tab
	$(call get_best,$<,$@)

other.trans.fasta.best.tab: other.trans.fasta.tab
	$(call get_best,$<,$@)



# calculate some statistics about the number of
# best hits and the used database
define CALC_STAT
	$(call load_modules); \
	paste \
	<(cut -f1 <$1 | bsort | uniq | wc -l) \
	<(cut -f3 <$1 | bsort | uniq | wc -l) \
	| cat <(bawk -M $@ | cut -f2 | transpose) - \
	| cat <(blastdbcmd -db $2 -info) - >$3
endef


.META: %.stat
	3	total queries with hit
	4	total subjects with hit

self.trans.fasta.best.stat: self.trans.fasta.best.tab other.trans.fasta.nin
	$(call CALC_STAT,$<,$(basename $^2),$@)

other.trans.fasta.best.stat: other.trans.fasta.best.tab self.trans.fasta.nin
	$(call CALC_STAT,$<,$(basename $^2),$@)




# ALGORITHM FOR FIND UNIQUE SWITCHED COUPLES:

# 1) lexicograpically sorting $$qacc and $$sacc in each row
# 2) sorting the 2 columns
# 3) take unique occurence of couples present > 1 time

# get_unique_couple:
# given a tab delimited list of unique couples from STDIN,
# extract one occurrence of the same couples (or swiched couples)
# plus the rest of line from a tab delimited list of redundant couples
# given as REDUNDANT_LST.
self-other.trans.fasta.best.reciprocal.tab: self.trans.fasta.best.tab other.trans.fasta.best.tab
	cat $^ \
	| bawk '{ if ( $$1 < $$3 ) { \   * compare qacc and 3 to lexicograpically sort them *
		print $$1, $$3; } \
	else { \
		print $$3, $$1; } \
	}' \
	| bsort -k 1,2 \
	| uniq -c \
	| tr -s ' ' \\t \
	| sed -e 's/^[ \t]*//' \   * remove tab begin of line *
	| bawk '{ if ( $$1 > 1 ) { print $$2, $$3; } }' \
	| get_unique_couple $< >$@



self.trans.fasta.best.single.tab: self-other.trans.fasta.best.reciprocal.tab self.trans.fasta.best.tab
	filter_1col -v 1 <(cut -f1 $<) <$^2 >$@

other.trans.fasta.best.single.tab: self-other.trans.fasta.best.reciprocal.tab other.trans.fasta.best.tab
	filter_1col -v 1 <(cut -f3 $<) <$^2 >$@

self.trans.nohit.fasta: self.trans.fasta self.trans.fasta.best.tab
	fasta2tab <$< \
	| filter_1col -v 1 <(cut -f1 $^2) \
	| bsort \
	| tab2fasta 2 >$@

other.trans.nohit.fasta: other.trans.fasta other.trans.fasta.best.tab
	fasta2tab <$< \
	| filter_1col -v 1 <(cut -f1 $^2) \
	| bsort \
	| tab2fasta 2 >$@


self-other.stats: self-other.trans.fasta.best.reciprocal.tab self.trans.fasta.best.single.tab self.trans.nohit.fasta other.trans.fasta.best.single.tab other.trans.nohit.fasta
	reciprocal=$$(cat $< | wc -l); \
	self=$$(echo "$^3" | cut -d'.' -f1); \
	self_single=$$(cat $^2 | wc -l); \
	self_nohit=$$(cat $^3 | fasta_count -s); \
	other=$$(echo "$^5" | cut -d '.' -f1); \
	other_single=$$(cat $^4 | wc -l); \
	other_nohit=$$(cat $^5 | fasta_count -s); \
	printf "#sample	reciprocal	single	nohit	total\n" >$@; \
	printf "$$self	$$reciprocal	$$self_single	$$self_nohit$$(echo "$$reciprocal + $$self_single + $$self_nohit" | bc)\n" >>$@; \
	printf "$$other	$$reciprocal	$$other_single	$$other_nohit$$(echo "$$reciprocal + $$other_single + $$other_nohit" | bc)\n" >>$@








# use union-find algorithm to group together couples that satisfy the transitive property:
# example, transform:
	# a = b
	# c = d
	# a = s
	# z = q
# to:
	# a = b = s
	# c = d
	# z = q

# These groups of couples define Orthogroup

# From: https://github.com/davidemms/OrthoFinder

# An orthogroup is the natural extension of the concept of orthology to groups of species.
# An orthogroup is the group of genes descended from a single gene in the Last Common Ancestor of a group of species
 
# Since all branching events in a gene tree are either speciation events (that give rise to orthologues) or duplication events 
# (that give rise to paralogues), any genes in the same orthogroup that are not orthologues must necessarily be paralogues.
 
# If you followed the explanations above it will be clear that an orthogroup is just a gene family/clade of genes defined at a specific taxonomic 
# level—namely, those genes descended from a single gene at the time of the LCA. Some
# Some may regard this definition of an orthogroup as unsatisfactory since an orthogroup can contain genes that are paralogues of one another 
# (ChA1 is a paralogue of ChA2 in Figure 2). However, this definition of an orthogroup is the only logically consistent way of 
# extending the concept of orthology to multiple species. If there have been gene duplication events it is not possible to create 
# a group of genes containing all orthologues and only orthologues—try it with the example above!
self-other.trans.fasta.orthogroups.tab: self.trans.fasta.best.tab other.trans.fasta.best.tab
	cat $^ \
	| get_transitive_couple \
	| sort -r -n \
	| bawk '{$$1=""; print $$0}' \
	| sed -e 's/^[ \t]*//' \
	| while read line; do \   * sort fields in each line *
		sorted=$$(sort -g -r -- <<< "$${line//[$$'\t', ]/$$'\n'}"); \
		printf -- "$${sorted//$$'\n'/$$'\t'}\n"; \
	done >$@


.META: self-other.orthogroups.stat
	1	nocc	number of occurrences
	2	glen	number of sequence in the group

self-other.orthogroups.stat: self.trans.fasta.best.tab other.trans.fasta.best.tab
	cat $^ \
	| get_transitive_couple \
	| cut -f1 \
	| bsort -r -n \
	| uniq -c \
	| tr -s ' ' \\t \
	| sed -e 's/^[ \t]*//' >$@


.PHONY: test
test:
	@echo 

ALL += self-other.trans.fasta.best.reciprocal.tab \
	self.trans.fasta.best.single.tab \
	self.trans.nohit.fasta \
	other.trans.fasta.best.single.tab \
	other.trans.nohit.fasta \
	self-other.trans.fasta.orthogroups.tab \
	self-other.orthogroups.stat \
	self.trans.fasta.best.stat \
	other.trans.fasta.best.stat \
	self-other.stats \
	
INTERMEDIATE += 

CLEAN += 

