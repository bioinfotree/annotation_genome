# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

# gff3 from previous maker-p run
MAKER_GFF3 := ../phase_4/annot.gff3.gz


log:
	mkdir -p $@

tmp:
	mkdir -p $@


query.fasta:
	ln -sf $(QUERY) $@

BY_CHR_GFF = $(addsuffix .annot.gff3,$(shell zcat $(MAKER_GFF3) | cut -f1  | bsort | uniq))
#BY_CHR_GFF = chr1.annot.gff3
# chr10.annot.gff3 chr10_random.annot.gff3 chr11.annot.gff3 chr11_random.annot.gff3 chr12.annot.gff3 chr12_random.annot.gff3 chr13.annot.gff3 chr13_random.annot.gff3 chr14.annot.gff3 chr15.annot.gff3 chr16.annot.gff3 chr16_random.annot.gff3 chr17.annot.gff3 chr17_random.annot.gff3 chr18.annot.gff3 chr18_random.annot.gff3 chr19.annot.gff3 chr1_random.annot.gff3 chr2.annot.gff3 chr3.annot.gff3 chr3_random.annot.gff3 chr4.annot.gff3 chr4_random.annot.gff3 chr5.annot.gff3 chr5_random.annot.gff3 chr6.annot.gff3 chr7.annot.gff3 chr7_random.annot.gff3 chr8.annot.gff3 chr9.annot.gff3 chr9_random.annot.gff3 chrUn.annot.gff3
%.annot.gff3:
	zcat $(MAKER_GFF3) \
	| bawk '! /^[\#+,$$]/ { print $$0 >$$1".annot.gff3"  }'


# CALCULATE STATISTICA FOR ANNOTATIONS
STATS = $(BY_CHR_GFF:.annot.gff3=.annot.gff3.stat)
%.annot.gff3.stat: %.annot.gff3
	$(call load_modules); \
	gt gff3 -tidy -sort -addintrons $< \
	2>/dev/null \
	| gt stat -o $@ \
	-force \   * force overwrite *
	-genelengthdistri \
	-exonlengthdistri \
	-exonnumberdistri \
	-intronlengthdistri \
	-cdslengthdistri


# temporary files
%.gene_length_dist.txt: %.annot.gff3.stat
	bawk '/gene length distribution/{flag=1;next}/exon length distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

%.exon_length_dist.txt: %.annot.gff3.stat
	bawk '/exon length distribution/{flag=1;next}/exon number distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

%.exon_number_distr.txt: %.annot.gff3.stat
	bawk '/exon number distribution/{flag=1;next}/intron length distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

%.intron_length_dist.txt: %.annot.gff3.stat
	bawk '/intron length distribution/{flag=1;next}/CDS length distribution/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@

%.CDS_length_distr.txt: %.annot.gff3.stat
	bawk '/CDS length distribution/{flag=1;next}/$^/{flag=0}flag' $< \
	| sed -e 's/://' -e 's/ /\t/g' \
	| cut -f 1,2 >$@


# plots
PLOTS = $(BY_CHR_GFF:.annot.gff3=.annot.gff3.stat.jpg)
%.annot.gff3.stat.jpg: %.gene_length_dist.txt %.exon_length_dist.txt %.exon_number_distr.txt %.intron_length_dist.txt %.CDS_length_distr.txt
	cat <<'EOF' | bRscript -
	
	options(width=10000)
	setwd("$(PWD)")
	
	#pdf("$@", onefile=T)
	jpeg("$@", width=6.61, height=10, res=1600, units='in', type="cairo")
	
	gene_length = bread(file="$<", header=F)
	exon_length = bread(file="$^2", header=F)
	exon_number = bread(file="$^3", header=F)
	intron_length = bread(file="$^4", header=F)
	cds_length = bread(file="$^5", header=F)
	
	
	par(mfrow=c(3,2)) # Divide the graphics window into N x M matrix
	
	gene_length_num = rep( gene_length[[1]], gene_length[[2]] ) # repeat each element in fisrt vector the number of time in the second vector
	bins = c( seq(from=1, to=max(gene_length[[1]]), by=400), max(gene_length[[1]]) )
	
	
	suppressWarnings(hist(	gene_length_num,
							main="Gene length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,4500),
							xlim=c(0,20000),
							freq=T,
							cex.axis=0.7
						))
	
	mtext("$(call first,$(SAMPLES))", side=3)


	#####
	exon_length_num = rep( exon_length[[1]], exon_length[[2]] )
	bins = c( seq(from=1, to=max(exon_length[[1]]), by=50), max(exon_length[[1]]) )
	
	suppressWarnings(hist(	exon_length_num,
							main="Exon length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,85000),
							xlim=c(0,2000),
							freq=T,
							cex.axis=0.7
						))
	
	####
	exon_number_num = rep( exon_number[[1]], exon_number[[2]] )
	bins = c( seq(from=1, to=max(exon_number[[1]]), by=1) )
	
	suppressWarnings(hist(	exon_number_num,
							main="Exon number ditribution",
							xlab="exon number #",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,15000),
							xlim=c(0,30),
							freq=T,
							cex.axis=0.7
						))

	####	
	intron_length_num = rep( intron_length[[1]], intron_length[[2]] ) # repeat each element in fisrt vector the number of time in the second vector
	bins = c( seq(from=1, to=max(intron_length[[1]]), by=100), max(intron_length[[1]]) )
	
	suppressWarnings(hist(	intron_length_num,
							main="Intron length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,60000),
							xlim=c(0,5000),
							freq=T,
							cex.axis=0.7
						))
						
	####	
	cds_length_num = rep( cds_length[[1]], cds_length[[2]] ) # repeat each element in fisrt vector the number of time in the second vector
	bins = c( seq(from=1, to=max(cds_length[[1]]), by=100), max(cds_length[[1]]) )
	
	suppressWarnings(hist(	cds_length_num,
							main="CDS length ditribution",
							xlab="lenght (bp)",
							ylab="#",
							border="black",
							col="blue",
							breaks=bins,
							ylim=c(0,3500),
							xlim=c(0,5000),
							freq=T,
							cex.axis=0.7
						))		
	dev.off()
	
	stats = list(	"gene_length"=gene_length_num, 
					"exon_length"=exon_length_num,
					"exon_number"=exon_number_num,
					"intron_length"=intron_length_num,
					"cds_length"=cds_length_num
				)
	
	# add total lenght to matrix returned by sapply
	with_len = rbind( 	sapply(stats, summary), c( 	sum(gene_length_num),
													sum(exon_length_num),
													sum(exon_number_num),
													sum(intron_length_num),
													sum(cds_length_num)     
												  )  
					)
	row.names(with_len)[7] = c("Tot.")
	
	# get some basic statistics
	bwrite( with_len, file="$(basename $@).summary1", row.names=T, col.names=T )
	EOF



SUMMARY = $(BY_CHR_GFF:.annot.gff3=.annot.gff3.stat.summary)
%.annot.gff3.stat.summary: %.annot.gff3.stat %.annot.gff3.stat.jpg
	bawk 'BEGIN { flag=1;} /gene length distribution/{flag=0}flag' $< \
	| sed -e 's/: /\t/g' -e 's/ (/\t/' -e 's/)//' \
	| cut -f 1,2 >$@


annot.gff3.stat.summary: $(SUMMARY) query.fasta
	REVERSE_FILE_LIST=$$(echo "$(SUMMARY)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')
	
	cut -f 1 $< >$@
	
	TMP=$$(mktemp -u tmp.XXX)
	for F in $(SUMMARY); do\
		TMP=$$(translate -a "$$F" 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@
	
	# start en R script
	cat <<'EOF' | bRscript -
	
	options(width=10000)
	setwd("$(PWD)")
	
	summary=bread(file="$@", header=T, row.names=1)
	
	# execute a system command and pass the stdout to a vector of characters
	fasta_len=system2( "fasta_length",
						args=c("<","query.fasta"),
						stdout=T,
						wait=T )
	
	# parse the vector of characters by splitting the lines for "\t"
	# then  cast the table to a dataframe taking row names from the first column
	chr_length=data.frame(  do.call(rbind, strsplit(fasta_len, "\t", fixed=T)),
							row.names=1 )
	
	# change col names
	colnames(chr_length)=c("chr length")
	
	# transpose the first dataframe,
	# them merge the 2 dataframes by row names
	# then print to file
	bwrite( merge( t(summary), chr_length, by=0, all=T),
			file="$@",
			row.names=F,
			col.names=T )
	
	EOF


	


.PHONY: test
test:
	@echo $(BY_CHR_GFF)


ALL += query.fasta \
	$(PLOTS) \
	annot.gff3.stat.summary

INTERMEDIATE += *.CDS_length_distr.txt \
	*.exon_length_dist.txt \
	*.exon_number_distr.txt \
	*.gene_length_dist.txt \
	*.intron_length_dist.txt

CLEAN += $(BY_CHR_GFF) \
	$(STATS) \
	$(SUMMARY)