# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>


.ONESHELL:

extern ../phase_4/id.transcripts.fasta.gz as MAKER_TRANS

query.trans.fasta:
	zcat <$(MAKER_TRANS) >$@

b2go.annot:
	ln -sf $(B2GO_BY_SEQ_ANNOT) $@

b2go.kegg:
	ln -sf $(B2GO_KEGG) $@



##### search against refseq plant
refseq_plant_protein: query.trans.fasta
	module purge; \
	module load lang/python/2.7.3; \
	mkdir -p refseq_plant_protein; \
	cd refseq_plant_protein; \
	ln -sf $$PRJ_ROOT/local/share/rules/parallel.blast.ptp .; \
	ln -sf ../$< .; \
	pipeline_mgmt launch flow parallel.blast.ptp \
	--label "$@.parallel.blast" \
	--skip-steps=s1,s2,s3 \
	--templates \
	blastcmd=blastx \
	db_base=/iga/biodb/ncbi/blastdb/refseq/83/plant/ \
	db=refseq_plant_protein \
	aln_limit=$(MAX_TARGET_SEQS) \
	eval=$(EVAL_BLAST) \
	fasta=$< \
	outdir=$$PWD; \
	cd ..

###### test if blast2go problem came from the new version of blast 2.6+
refseq_plant_protein_2.2.28: query.trans.fasta
	module purge; \
	module load lang/python/2.7.3; \
	mkdir -p refseq_plant_protein_2.2.28; \
	cd refseq_plant_protein_2.2.28; \
	ln -sf $$PRJ_ROOT/local/share/rules/parallel.blast.2.2.28.ptp .; \
	ln -sf ../$< .; \
	pipeline_mgmt launch flow parallel.blast.2.2.28.ptp \
	--label "$@.parallel.blast" \
	--skip-steps=s1,s4,s5,s6,s7 \
	--templates \
	blastcmd=blastx \
	db_base=/iga/biodb/ncbi/blastdb/refseq/83/plant/ \
	db=refseq_plant_protein \
	aln_limit=$(MAX_TARGET_SEQS) \
	eval=$(EVAL_BLAST) \
	fasta=$< \
	outdir=$$PWD; \
	cd ..

###### test if blast2go problem came from the newest ref_seq_plant database
refseq_plant_protein_nr: query.trans.fasta
	module purge; \
	module load lang/python/2.7.3; \
	mkdir -p refseq_plant_protein_nr; \
	cd refseq_plant_protein_nr; \
	ln -sf $$PRJ_ROOT/local/share/rules/parallel.blast.ptp .; \
	ln -sf ../$< .; \
	pipeline_mgmt launch flow parallel.blast.ptp \
	--label "$@.parallel.blast" \
	--skip-steps=s1,s4,s5,s6,s7 \
	--templates \
	blastcmd=blastx \
	db=nr \
	db_base=/iga/biodb/ncbi/blastdb/latest \
	aln_limit=$(MAX_TARGET_SEQS) \
	eval=$(EVAL_BLAST) \
	fasta=$< \
	outdir=$$PWD; \
	cd ..



# correct the xml file:

# NEW VERSION (not working with blast2go):
# <Hit_id>gnl|BL_ORD_ID|418002</Hit_id>
# <Hit_def>XP_002283946.2 PREDICTED: pentatricopeptide repeat-containing protein At5g27460 [Vitis vinifera]</Hit_def>
# <Hit_accession>418002</Hit_accession>

# OLD VERSION (working with blast2go):
# <Hit_id>gi|731428532|ref|XP_002283946.2|</Hit_id>
# <Hit_def>PREDICTED: pentatricopeptide repeat-containing protein At5g27460 [Vitis vinifera]</Hit_def>
# <Hit_accession>XP_002283946</Hit_accession>

# usefull suggestions about xml parsing with ElementTree can be found here:
# https://www.blog.pythonlibrary.org/2013/04/30/python-101-intro-to-xml-parsing-with-elementtree/
merged.xml: refseq_plant_protein
	$(call load_modules); \
	cat <<'EOF' | python -
	
	import sys
	import xml.etree.ElementTree as ET
	
	def main():
			tree = ET.parse('$<//merged.xml')
			root = tree.getroot()
			
			for id, hit_def, acc in zip(root.iter("Hit_id"), root.iter("Hit_def"), root.iter("Hit_accession")):
					access = hit_def.text.split()[0]
					new_id = 'gi|%s|ref|%s|' %(acc.text, access)
					new_def = ' '.join(hit_def.text.split()[1:])
					new_acc = access.split('.')[0]
					
					id.text = new_id
					hit_def.text = new_def
					acc.text = new_acc
	
			tree = ET.ElementTree(root)
			with open("$@", "w") as f:
					tree.write(f)
	
	if __name__ == '__main__':
			main()
	EOF


####### archive data
refseq_plant_protein.tar.pxz: refseq_plant_protein
	module load compression/pixz/1.0.2; \
	tar cvf - $< | pixz -p 8 -9 >$@
#######


# correspond to what is call tab1 in prj/annotation3
refseq_plant_protein.tab: refseq_plant_protein
	$(call load_modules); \
	cat $</splits/*.fasta.tab1 >$@



define CALC_STAT
	$(call load_modules); \
	paste \
	<(cut -f1 <$1 | bsort | uniq | wc -l) \
	<(cut -f3 <$1 | bsort | uniq | wc -l) \
	| cat <(bawk -M $@ | cut -f2 | transpose) - \
	| cat <(blastdbcmd -db $2 -info) - >$3
endef


.META: %.stat
	3	total queries with hit
	4	total subjects with hit

refseq_plant_protein.best.stat: refseq_plant_protein.best.tab
	$(call CALC_STAT,$<,/iga/biodb/ncbi/blastdb/refseq/83/plant/refseq_plant_protein,$@)




#### nr
nr: query.trans.fasta
	module purge; \
	module load lang/python/2.7.3; \
	mkdir -p $@; \
	cd $@; \
	ln -sf $$PRJ_ROOT/local/share/rules/parallel.blast.ptp .; \
	ln -sf ../$< .; \
	pipeline_mgmt launch flow parallel.blast.ptp \
	--label "$@.parallel.blast" \
	--templates blastcmd=blastx \
	db=nr \
	db_base=/iga/biodb/ncbi/blastdb/latest \
	fasta=$< \
	aln_limit=$(MAX_TARGET_SEQS) \
	eval=$(EVAL_BLAST) \
	outdir=$$PWD; \
	cd ..
#### nr







##### new vitis annotation
# the dataset that comprises the 12X.v2 grapevine reference genome assembly and its VCost.v3 gene annotation in Genbank format.
# Note that the 12X.v2 chromosome assembly is based on the scaffolds of the grapevine reference genome
# build (FN594950-FN597014, EMBL release 102; Vitis vinifera cv. PN40024).
vcost.v3.gb:
	wget -qO $@ https://urgi.versailles.inra.fr/files/Vini/Vitis%2012X.2%20annotations/Vitis_vinifera_gene_annotation_on_V2_10_no_CDS.gb

vcost.v3.gb.gff: vcost.v3.gb
	bp_genbank2gff3.pl --noinfer $<

vcost.v3.gb.gff.stat: vcost.v3.gb.gff
	$(call load_modules); \
	gt gff3 -tidy -sort -addintrons -fixregionboundaries -retainids vcost.v3.gb.gff


# join is used but is unusefull
# extract mrna sequences from genebank
# what about converting to gff3?
vcost.v3.trans.fasta: vcost.v3.gb
	$(call load_modules); \
	extractfeat $< -type 'mRNA' -describe 'note' -join -filter \
	| fasta2tab \
	| bawk '!/^[$$,\#]/ { \
	gsub ( "[,():'"'"'\"]","" ); \
	split($$1,a," "); \
	match($$1, /source.+/, b); \
	split(b[0],c," "); \
	match($$1, /Parent.+/, d); \
	split(d[0],e," "); \
	seq=toupper($$2); \
	print a[4]" name="a[6]" gene="e[2]" source="c[2], seq; }' \
	| bsort \
	| tab2fasta 2 >$@


vcost.v3.trans.fasta.nin: vcost.v3.trans.fasta
	$(call load_modules); \
	makeblastdb -in $< -dbtype nucl -title "vcost v3 annotations"


.PRECIOUS: vcost.v3.trans.asn.1
vcost.v3.trans.asn.1: query.trans.fasta
	!threads
	$(call load_modules); \
	blastn -evalue $(EVAL_BLAST) -num_threads $$THREADNUM -outfmt 11 -max_target_seqs $(MAX_TARGET_SEQS) -query $< -db /projects/novabreed/share/mvidotto/bioinfotree/prj/annotation_genome/dataset/cfranc16_2/phase_7/vcost.v3.trans.fasta -out $@
	
vcost.v3.trans.xml: vcost.v3.trans.asn.1
	$(call load_modules); \
	blast_formatter -archive $< -outfmt 5 -out $@
	
# correspond to what is call tab1 in prj/annotation3
vcost.v3.trans.tab: vcost.v3.trans.asn.1
	$(call load_modules); \
	blast_formatter -archive $< -outfmt "6 qacc qlen sacc slen evalue qstart qend sstart send length" -out $@

vcost.v3.trans.best.stat: vcost.v3.trans.tab
	$(call CALC_STAT,$<,/projects/novabreed/share/mvidotto/bioinfotree/prj/annotation_genome/dataset/cfranc16_2/phase_7/vcost.v3.trans.fasta,$@)


	
	


##### search V 2.1 mRNA annotation
v2.1.b2go.annot:
	wget -O $@ http://genomes.cribi.unipd.it/DATA/V2/annotation/bl2go.annot_with_GO_description.txt

.PRECIOUS: v2.1.trans.asn.1
v2.1.trans.asn.1: query.trans.fasta
	!threads
	$(call load_modules); \
	blastn -evalue $(EVAL_BLAST) -num_threads $$THREADNUM -outfmt 11 -max_target_seqs $(MAX_TARGET_SEQS) -query $< -db /projects/vitis/genes/sequences/V2.1/V2.1.mRNA.fa -out $@
	
v2.1.trans.xml: v2.1.trans.asn.1
	$(call load_modules); \
	blast_formatter -archive $< -outfmt 5 -out $@

v2.1.trans.tab: v2.1.trans.asn.1
	$(call load_modules); \
	blast_formatter -archive $< -outfmt "6 qacc qlen sacc slen evalue qstart qend sstart send length" -out $@

v2.1.trans.best.stat: v2.1.trans.best.tab
	$(call CALC_STAT,$<,/projects/vitis/genes/sequences/V2.1/V2.1.mRNA.fa,$@)

	
	



##### select best match and statistics
.META: %.tab %.best.tab
	1	qacc	Query accesion
	2	qlen	Query sequence length
	3	sacc	Subject accession
	4	slen	Subject sequence length
	5	evalue	Expect value
	6	qstart	Start of alignment in query
	7	qend	End of alignment in query
	8	sstart	Start of alignment in subject
	9	send	End of alignment in subject
	10	length	Alignment length

%.best.tab: %.tab
	bawk 'BEGIN { i=0; start=""; } !/^[\#,$$]/ { \
	if ( NR == 1 ) { \
		start=$$qacc; \
		if ( $$evalue <= $(EVAL_FILTER) ) { print $$0; } \
		} \
	else { \
		if ( $$qacc != start ) { \
			start=$$qacc; \
			if ( $$evalue <= $(EVAL_FILTER) ) { print $$0; } \
			} \
		} \
	}' $< >$@



	

##### blast2go annotations
annot.info: b2go.annot
	paste -d "\n" \
	<(TIPE="P" && \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { if ( $$3 == type ) { print $$0 }; }' $< \
	| cut -f 4 \
	| bsort \
	| uniq -c \
	| wc -l \
	| bawk -v type=$$TIPE '!/^[$$,\#+]/ { printf "unique_%s_terms\t%i", type, $$0; }') \
	<(TIPE="C" && \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { if ($$3 == type) { print $$0; }; }' $< \
	| cut -f 4 \
	| bsort \
	| uniq -c \
	| wc -l \
	| bawk -v type=$$TIPE '!/^[$$,\#+]/ { printf "unique_%s_terms\t%i", type, $$0; }') \
	<(TIPE="F" && \
	bawk -v type=$$TIPE '!/^[$$,\#+]/ { if ($$3 == type) { print $$0; } }' $< \
	| cut -f 4 \
	| bsort \
	| uniq -c \
	| wc -l \
	| bawk -v type=$$TIPE '!/^[$$,\#+]/ { printf "unique_%s_terms\t%i", type, $$0; }') >$@


KEGG.info: b2go.kegg
	paste -d "\n" \
	<(unhead <$< \
	| cut -f 1,7 $ \
	| bsort --key=2,2 \
	| uniq -c \
	| bsort --key=1,1 \
	| wc -l \
	| bawk '!/^[$$,\#+]/ { print "unique_KEGG_pathways", $$0; }') \
	<(unhead $< \
	| cut -f 5 $< \
	| bawk '/^[0-9]/' \
	| stat_base --total 1 \
	| bawk '!/^[$$,\#+]/ { print "total_sequences", $$0; }') \
	<(bawk '!/^[$$,\#+]/ { if ( $$4 ~ /^ec/ ) print $$4; }' $< \
	| bsort \
	| uniq -c \
	| wc -l \
	| bawk '!/^[$$,\#+]/ { print "total_EC_codes", $$0; }') >$@


sorted_pathway_crowding: b2go.kegg
	unhead $< \
	| cut -f 1,7 \
	| bsort --key=2,2 \
	| tr -s " " "_" \
	| uniq -c \
	| bsort --key=1,1 \
	| tr -s " " \\\t \
	| tr -s "_" " " >$@




ALL += refseq_plant_protein \
	refseq_plant_protein.best.tab \
	refseq_plant_protein.best.stat \
	vcost.v3.trans.best.tab \
	vcost.v3.trans.best.stat \
	v2.1.trans.best.tab \
	v2.1.trans.best.stat \
	annot.info \
	KEGG.info
	

INTERMEDIATE +=

CLEAN += refseq_plant_protein.tab \
	v2.1.trans.tab \
	vcost.v3.trans.tab
