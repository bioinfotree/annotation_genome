# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>

extern ../phase_4/id.proteins.fasta.gz as MAKER_PROT


interproscan.profile.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/interproscan.profile.ptp $@


## restore the initial name of the sequences
query.pep.fasta:
	zcat <$(MAKER_PROT) >$@


.PHONY: launch
launch: interproscan.profile.ptp query.pep.fasta
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--label "$@.interproscan" \
	--templates fasta=$$PWD/$^2 outdir=$$PWD prefix=query

ALL += interproscan.profile.ptp \
	query.pep.fasta \
	launch

INTERMEDIATE +=

CLEAN += query.pep.fasta