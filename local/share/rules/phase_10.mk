# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

extern ../phase_4/id.proteins.fasta.gz as SELF_PROT

OTHER_PROT := /projects/novabreed/share/mvidotto/bioinfotree/prj/annotation_genome/dataset/cfranc16_2/phase_4/id.proteins.fasta.gz

self.prot.fasta:
	zcat <$(SELF_PROT) >$@

other.prot.fasta:
	zcat <$(OTHER_PROT) >$@

# self.prot.fasta:
	# ln -sf $$PRJ_ROOT/local/src/OrthoFinder-1.1.8/ExampleData/Mycoplasma_agalactiae.faa $@

# other.prot.fasta:
	# ln -sf $$PRJ_ROOT/local/src/OrthoFinder-1.1.8/ExampleData/Mycoplasma_gallisepticum.faa $@


01_test: self.prot.fasta other.prot.fasta
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load compilers/gcc/4.9.3; \
	module load aligners/blast/2.6.0+; \
	ln -sf $$PRJ_ROOT/local/src/OrthoFinder-1.1.8/config.json .; \
	orthofinder -f $$PWD --threads 8 --algthreads 8 --only-groups

02_test: self.prot.fasta other.prot.fasta
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load compilers/gcc/4.9.3; \
	module load aligners/blast/2.6.0+; \
	ln -sf $$PRJ_ROOT/local/src/OrthoFinder-1.1.8/config.json .; \
	orthofinder -f $$PWD --threads 8 --algthreads 8



.PHONY: test
test:
	@echo 

ALL += 

INTERMEDIATE += 

CLEAN += 
