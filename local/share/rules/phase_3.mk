# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# gff3 from previous maker-p run
extern ../phase_2.old/final_maker.gff as MAKER_GFF3

# for testing purpose
query.fasta:
	error_ignore \
	cat <$(QUERY) \
	| fasta2tab \
	| head -n 1 \
	| tab2fasta 2 >$@

# maker iga pipeline
augustus.training.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/snap_augustus_training_annotation.ptp $@


.PHONY: launch
launch: augustus.training.ptp query.fasta
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--label "$(call first,$(SAMPLES)).augustus" \
	--templates \
	reference=$^2 \
	maker_gff=$(abspath $(MAKER_GFF3)) \
	model_name="vitis_vinifera_$(call first,$(SAMPLES))" \
	outdir=$(PWD)


.PHONY: test
test:


ALL += 

INTERMEDIATE += 

CLEAN +=