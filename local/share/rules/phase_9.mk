# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

extern ../phase_4/id.proteins.fasta.gz as SELF_PROT

OTHER_PROT := /projects/novabreed/share/mvidotto/bioinfotree/prj/annotation_genome/dataset/cfranc16_2/phase_4/id.proteins.fasta.gz

self.prot.fasta:
	zcat <$(SELF_PROT) >$@

other.prot.fasta:
	zcat <$(OTHER_PROT) >$@

# self.prot.fasta:
	# ln -sf $$PRJ_ROOT/local/src/inparanoid_4.1/EC $@

# other.prot.fasta:
	# ln -sf $$PRJ_ROOT/local/src/inparanoid_4.1/SC $@

01_test: self.prot.fasta other.prot.fasta
	$(call load_modules); \
	ln -sf "$$PRJ_ROOT/local/src/inparanoid_4.1/BLOSUM80" .; \
	ln -sf "$$PRJ_ROOT/local/src/inparanoid_4.1/blast_parser.pl" .; \
	ln -sf "$$PRJ_ROOT/local/src/inparanoid_4.1/seqstat" .; \
	inparanoid.pl $< $^2


.PHONY: test
test:
	@echo 

ALL += 

INTERMEDIATE += 

CLEAN += error.log \
orthologs.self.prot.fasta-other.prot.fasta.html \
other.prot.fasta \
other.prot.fasta-other.prot.fasta \
other.prot.fasta-self.prot.fasta \
Output.self.prot.fasta-other.prot.fasta \
self.prot.fasta \
self.prot.fasta-other.prot.fasta \
self.prot.fasta-self.prot.fasta \
sqltable.self.prot.fasta-other.prot.fasta \
table.self.prot.fasta-other.prot.fasta

