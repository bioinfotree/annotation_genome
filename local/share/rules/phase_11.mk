# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>


extern ../phase_4/id.transcripts.fasta.gz as SELF_TRANS
# need get_unique_couple and get_transitive_couple python scripts
context prj/line_date2

MAKE := bmake



##### search V 2.1 mRNA annotation
v2.1.b2go.annot:
	wget -O $@ http://genomes.cribi.unipd.it/DATA/V2/annotation/bl2go.annot_with_GO_description.txt

v2.1.trans.fasta.gz:
	fasta2tab </projects/vitis/genes/sequences/V2.1/V2.1.mRNA.fa \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { print $$1,$$NF; }' \   * remove all after the first space *
	| bsort \
	| tab2fasta 2 \
	| gzip -c >$@
#####



##### new vitis annotation
# the dataset that comprises the 12X.v2 grapevine reference genome assembly and its VCost.v3 gene annotation in Genbank format.
# Note that the 12X.v2 chromosome assembly is based on the scaffolds of the grapevine reference genome
# build (FN594950-FN597014, EMBL release 102; Vitis vinifera cv. PN40024).
vcost.v3.gb:
	wget -qO $@ https://urgi.versailles.inra.fr/files/Vini/Vitis%2012X.2%20annotations/Vitis_vinifera_gene_annotation_on_V2_10_no_CDS.gb

# join is used but is unusefull
# extract mrna sequences from genebank
# what about converting to gff3?
vcost.v3.trans.fasta: vcost.v3.gb
	$(call load_modules); \
	extractfeat $< -type 'mRNA' -describe 'note' -join -filter \
	| fasta2tab \
	| bawk '!/^[$$,\#]/ { \
	gsub ( "[,():'"'"'\"]","" ); \
	split($$1,a," "); \
	match($$1, /source.+/, b); \
	split(b[0],c," "); \
	match($$1, /Parent.+/, d); \
	split(d[0],e," "); \
	seq=toupper($$2); \
	print a[4], seq; }' \   * retain only ID and seq *
	| bsort \
	| tab2fasta 2 >$@

vcost.v3.prot.fasta: vcost.v3.trans.fasta
	$(call load_modules); \
	gt seqtranslate $< >$@
#####



# integrate local taget
local_target.mk: v2.1.trans.fasta.gz
	#echo -e "vcost.v3 := $$(readlink -sf $<)" >$@
	echo -e "v2.1 := $$(readlink -sf $<)" >>$@
	
include local_target.mk






# this function install all the links at once
%.trans.fasta.gz %.self.gene.del.ref.bed %.other.gene.del.ref.bed:
		@echo installing links ... \
		$(foreach SAMPLE,$(SAMPLES1), \
				$(shell ln -sf $(call get,$(SAMPLE),TRANS) $(SAMPLE).trans.fasta.gz) \
				$(shell ln -sf $(call get,$(SAMPLE),SELF_GENE_COV) $(SAMPLE).self.gene.del.ref.bed) \
				$(shell ln -sf $(call get,$(SAMPLE),SAMPLE_GENE_COV) $(SAMPLE).other.gene.del.ref.bed) \
		) \
		&& sleep 3


# add all comparisons to be done
COMPARISONS := $(addsuffix .self,$(COMPARE) v2.1)

self.trans.fasta:
	zcat <$(SELF_TRANS) >$@


%.self: makefile self.trans.fasta %.trans.fasta.gz %.self.gene.del.ref.bed %.other.gene.del.ref.bed
	!threads
	$(call load_modules); \
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_11.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
		mkdir -p $@; \
		cd $@; \
		ln -sf ../$<; \
		ln -sf $$RULES_PATH rules.mk; \
		ln -sf ../$^2 self.trans.fasta; \
		if [[ ../$^3 =~ \.t?gz$$ ]]; then \   * test if file is gzipped *
			zcat <../$^3 >other.trans.fasta;  \
		else \
			ln -sf ../$^3 other.trans.fasta; \
		fi; \
		ln -sf ../$^4 self.gene.del.ref.bed; \
		ln -sf ../$^5 other.gene.del.ref.bed; \
		#$(MAKE) -j $$THREADNUM; \   * use 8 threads *
		cd ..; \
	fi



# collect all the statistics
%.self.stats: %.self
	if [ -s $</self-other.stats ]; then \
		cat $</self-other.stats \
		| sed -e 's/other/$*/' >$@ ; \
	fi

# put all stats together
self.stats.all: $(addsuffix .self.stats,$(COMPARE))
	cat $^ >$@




BEST_RECIPROCAL = $(addsuffix .reciprocal.tab,$(COMPARE))
%.reciprocal.tab: %.self
	if [ -s $</self-other.trans.fasta.best.reciprocal.tab ]; then \
		cat $</self-other.trans.fasta.best.reciprocal.tab >$@ ; \
	fi


reciprocal.tab.all: $(BEST_RECIPROCAL)
	cat $^ \
	| get_transitive_couple \
	| sort -r -n \
	| bawk '{$$1=""; print $$0}' \
	| sed -e 's/^[ \t]*//' \
	| while read line; do \   * sort fields in each line *
		sorted=$$(sort -g -r -- <<< "$${line//[$$'\t', ]/$$'\n'}"); \
		printf -- "$${sorted//$$'\n'/$$'\t'}\n"; \
	done >$@


BEST = $(addsuffix .best.tab,$(COMPARE))
%.best.tab: %.self
	if [ -s $</self.trans.fasta.best.tab ] && [ -s $</other.trans.fasta.best.tab ]; then \
		cat $</self.trans.fasta.best.tab $</other.trans.fasta.best.tab >$@ ; \
	fi







.PHONY: test
test:
	@echo

ALL += $(COMPARISONS) \
	$(addsuffix .self.stats,$(COMPARE)) \
	self.stats.all
	
ALL += $(BEST_RECIPROCAL) \
	$(BEST)

# ALL += reciprocal.tab.all \
	# reciprocal_single.tab.stats \
	# reciprocal.tab.stats
	
INTERMEDIATE += 

CLEAN += 

