# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>


logs:
	mkdir -p $@

default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/$@

# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
RG_FASTQ_GZ = $(addsuffix .RG,$(SAMPLES))
%.1.fastq.gz  %.2.fastq.gz  %.RG:
		@echo installing links ... \
		$(foreach SAMPLE,$(SAMPLES), \
				$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
				$(shell echo -E '$(call get,$(SAMPLE),RG)' | sed -e 's/\bRG..\b/\L&/g' >$(SAMPLE).RG) \   * make RG.. to lowercase  to pass them to pipeline *
		) \
		&& sleep 3



LAUNCH = $(addsuffix .launch,$(SAMPLES))
TRANSCRIPTS_GTF = $(addsuffix /cufflinks_denovo/transcripts.gtf,$(SAMPLES))

# launch pipeline  for:
# 1. align to the reference with tophat2
# 2. add read groups with picard
# 3. assemble transcripts with cufflinks_denovo without gtf guidance
# 4. calculate some statistics
.PHONY: %.launch
%.launch: default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp %.1.fastq.gz %.RG
	!threads
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--skip-steps=s3,s8 \   * skip cufflinks_strict, cuffquant *
	--label "$@.$(basename $<)" \
	--templates \
	library_type=$(call get,$*,ST) \
	output_dir="$$PWD" \
	output_prefix="$(basename $@)" \
	reference_sequence="$(QUERY)" \
	read_1=$^2 \
	threads=$$THREADNUM \
	$(shell cat $^3)

# merge gene annotation from different rnaseq libraries into
# single annotation
cuffmerge/merged.gtf: logs
	!threads
	$(call load_modules); \
	cuffmerge \
	-o cuffmerge \
	-p "$$THREADNUM" \
	-s $(QUERY) \
	<(echo "$(addprefix $(PWD)/,$(TRANSCRIPTS_GTF))" | tr ' ' '\n') \   * generate transcripts list *
	2>&1 \
	>$</$(notdir $@).log

# convert cuffmerge gtf to gff3 for maker
merged.gff3: logs cuffmerge/merged.gtf
	$(call load_modules); \
	export PATH=/iga/scripts/dev_modules/src/sw/bio/maker/maker-2.31.8/bin:$PATH; \
	cufflinks2gff3 $^2 >$@ \
	2>$</$@.log

# find . -name "*.tab" -type f | parallel --verbose --progress --jobs 6 "gzip -c9 <{} >{}.gz && if [ -s {}.gz ]; then rm -f {}; fi"

.PHONY: test
test:
	@echo $(TRANSCRIPTS_GTF)

ALL += default-unified_rna-seq_v1_tophat2-cufflinks-stats.ptp \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(RG_FASTQ_GZ) \
	$(LAUNCH)

INTERMEDIATE +=

CLEAN += 