# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

context prj/line_date2

pn_ref.gene.del.ref.bed:
		ln -sf $(call get,pn_ref,SELF_GENE_COV) $@

# genes uncovered by pn_ref
gene.uncovered.bed: pn_ref.gene.del.ref.bed
	bawk '{ if ( $$20 == "1" ) print $$0 }' $< >$@

# these are considered unique genes of the sample
gene.uncovered.all.samples: ../../datasets/$(SELF)/annot.matrix.bed
	awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
		if ( $$8 == "gene" ) { \
			del=0; \
			for (i=15; i<=NF; i=i+3) { \   * genes every 3 columns *
				if ( $$i == "1" ) del++; \ 
			} \
			if ( del == ((NF - 15 + 1) / 3) ) print $$0; \
		} \
	}' <$< >$@


.PHONY: test
test:
	@echo

ALL += pn_ref.gene.del.ref.bed \
	gene.uncovered.bed \
	gene.uncovered.all.samples

INTERMEDIATE += 

CLEAN += 