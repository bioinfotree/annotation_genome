# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# gff3 from transcriptomes
extern ../phase_1/merged.gff3 as CUFF_GFF3

REPBASE ?= http://www.girinst.org/server/RepBase/protected/REPET/RepBase20.05_REPET.embl.tar.gz

# for testing purpose
# query.fasta:
	# error_ignore \
	# cat <$(QUERY) \
	# | fasta2tab \
	# | head -n 1 \
	# | tab2fasta 2 >$@

query.fasta:
	ln -sf $(QUERY) $@

# ### FOR TESTING PURPOSE
# # cDNA from Ensembl based on a 12x whole genome shotgun sequence assembly and the V1 annotation of the Vitis vinifera genome
# # 29971: same number as Nucleotide GSS (Genome survey sequence) in NCBI: 229,272
# e.cdna.fasta:
	# wget -O - ftp://ftp.ensemblgenomes.org/pub/plants/release-34/fasta/vitis_vinifera/cdna/Vitis_vinifera.IGGP_12x.cdna.all.fa.gz \
	# | zcat >$@

# # 26346: V0 annotation of the 12X.0 genome assembly (Genoscope)
# v0.urgi.cdna.fasta:
	# wget -O - https://urgi.versailles.inra.fr/content/download/2158/19380/file/Vitis_vinifera_mRNA.fa.gz \
	# | zcat >$@

# # 29971: V1 annotation files is divided in V1_NR (no repetitive genes) and V1_RO (repetitive genes, like genes family)
# v1.urgi.cdna.fasta:
	# wget -O - https://urgi.versailles.inra.fr/content/download/2163/19400/file/V1_JigsawGazeNR_mRNA.fa.gz \
	# | zcat >$@
# ###

### Download protein and nucleic acid data for repeats
# use ID and password of Gabriele
RepBaseREPET.embl.tar.gz:
	wget -q \
	--user=asso89 \
	--password=6e7bta \
	-O $@ \
	$(REPBASE)


# extract specific file using p$(PRJ)attern
repbase_aaSeq_cleaned_TE.fa: RepBaseREPET.embl.tar.gz
	tar -xvzf $< --to-stdout --wildcards --no-anchored '*_aa*' >$@

# repbase_ntSeq_cleaned_TE.fa: RepBaseREPET.embl.tar.gz
	# tar -xvzf $< --to-stdout --wildcards --no-anchored '*_nt*' >$@

# merge to create complete copy of the repeat db
repeats.fasta:
	cat /projects/vigneto/share/gmagris/50variety_SV/paper/SV/family_classification/TE_dario/db_vitis_whole-TE-only_renamed_3lettercode_n674.3letter.fasta \
	/projects/vigneto/share/gmagris/50variety_SV/paper/SV/family_classification/TE_dario/db_vitis_whole-TE-only_renamed_3lettercode_n674.repbase.fasta \
	| sed '/^$$/d' >$@
###



# merge preteins from NCBI and V2.1 annotation
ncbi_prots.fasta:
	cat /projects/novabreed/share/dscaglione/rkatsiteli_case-study/ESTs_Prot/ncbi_prots.fasta \
	/projects/vitis/genes/sequences/V2.1/V2.1.prot \
	| sed '/^$$/d' >$@




# maker iga pipeline
distributed.makerp.ptp:
	ln -sf $$PRJ_ROOT/local/share/rules/distributed_single-node_mpi_maker-p.ptp $@



# configurations are reported in makefile
export MAKER_CONF

maker_opts_setup.ctl:
	@echo "$$MAKER_CONF" >$@



.PHONY: launch
launch: distributed.makerp.ptp query.fasta maker_opts_setup.ctl ncbi_prots.fasta repeats.fasta repbase_aaSeq_cleaned_TE.fa
	module purge; \
	module load lang/python/2.7.3; \
	pipeline_mgmt launch flow $< \
	--skip-steps=s1 \   * skip s1 since the configuration file is now produced by the makefile *
	--label "$(call first,$(SAMPLES)).makerp" \
	--templates \
	input_fasta=$$PWD/$^2 \
	outdir=$$PWD

# --skip-steps=s1,s2,s3,s4,s5,s6,s7 \

final_maker.gff.gz:
	gzip -c9 <final_maker.gff >$@ 


.PHONY: test
	test


ALL += launch

INTERMEDIATE += 

CLEAN +=
