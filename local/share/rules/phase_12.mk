# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>



extern ../phase_4/id.proteins.fasta.gz as MAKER_PROT

tmp:
	mkdir -p $@

genome.fasta:
	ln -sf $(QUERY) $@
	
proteins.fasta:
	zcat <$(MAKER_PROT) >$@


# $(SELF).busco_genome: genome.fasta proteins.fasta tmp
	# GENOME=$$(readlink -sf $<); \
	# PROT=$$(readlink -sf $^2); \
	# TMP=$$(readlink -sf $^3); \
	# OUT=$$PWD; \
	# cd /projects/ciliegio/share/pinosio/busco/v3; \
	# module purge; \
	# module load lang/python/3.4; \
	# module load aligners/blast/2.2.28; \
	# module load repeats/hmmer/3.1b1; \
	# module load sw/bio/emboss/6.4.0; \
	# module load lang/r/3.3; \
	# export PATH=/iga/scripts/dev_modules/src/sw/bio/augustus/augustus.2.5.5/bin:$$PATH; \
	# export PATH=/iga/scripts/dev_modules/src/sw/bio/augustus/augustus.2.5.5/scripts:$$PATH; \
	# export AUGUSTUS_CONFIG_PATH=/iga/scripts/dev_modules/src/sw/bio/augustus/augustus.2.5.5/config/; \
	# python scripts/run_BUSCO.py -i $$GENOME -c 8 -o $@ -m genome -l embryophyta_odb9 -t $$TMP   * genome *

run_$(SELF).busco_genome: genome.fasta tmp
	module purge; \
	module load tools/busco/3.02b; \
	run_BUSCO.py -i $< -c 8 -o $@ -m genome -l /projects/novabreed/share/mvidotto/bioinfotree/binary/appliedgenomics/local/stow/busco-3.02b/embryophyta_odb9 -t $^2


# $(SELF).busco_prot: genome.fasta proteins.fasta tmp
	# GENOME=$$(readlink -sf $<); \
	# PROT=$$(readlink -sf $^2); \
	# TMP=$$(readlink -sf $^3); \
	# OUT=$$PWD; \
	# cd /projects/ciliegio/share/pinosio/busco/v3; \
	# module purge; \
	# module load lang/python/3.4; \
	# module load aligners/blast/2.2.28; \
	# module load repeats/hmmer/3.1b1; \
	# module load sw/bio/emboss/6.4.0; \
	# module load lang/r/3.3; \
	# export PATH=/iga/scripts/dev_modules/src/sw/bio/augustus/augustus.2.5.5/bin:$$PATH; \
	# export PATH=/iga/scripts/dev_modules/src/sw/bio/augustus/augustus.2.5.5/scripts:$$PATH; \
	# export AUGUSTUS_CONFIG_PATH=/iga/scripts/dev_modules/src/sw/bio/augustus/augustus.2.5.5/config/; \
	# python scripts/run_BUSCO.py -i $$PROT -c 8 -o $@ -m prot -l embryophyta_odb9 -t $$TMP   * proteins *


run_$(SELF).busco_prot: proteins.fasta tmp
	module purge; \
	module load tools/busco/3.02b; \
	run_BUSCO.py -i $< -c 8 -o $@ -m prot -l /projects/novabreed/share/mvidotto/bioinfotree/binary/appliedgenomics/local/stow/busco-3.02b/embryophyta_odb9 -t $^2

# generate plots using R script and ggplot2
# N.B.: must NOT be executed in screen mode
busco_plot:
	module purge; \
	module load tools/busco/3.02b; \
	mkdir -p $@; \
	cd $@; \
	cp ../run_$(SELF).busco_genome/short_summary_$(SELF).busco_genome.txt .; \
	cp ../run_$(SELF).busco_prot/short_summary_$(SELF).busco_prot.txt .; \
	generate_plot.py -wd $$PWD


run_$(SELF).busco_%.tar.pxz: 
	!threads
	module purge; \
	module load compression/pixz/1.0.2; \
	tar cvf - run_$(SELF).busco_$* | pixz -p $$THREADNUM -9 >$@

.PHONY: test
	test


# ALL += run_$(SELF).busco_genome \
	# run_$(SELF).busco_prot \
	# busco_plot \
	# run_$(SELF).busco_genome.tar.pxz \
	# run_$(SELF).busco_prot.tar.pxz

ALL += run_$(SELF).busco_genome.tar.pxz \
	run_$(SELF).busco_prot.tar.pxz

INTERMEDIATE +=

CLEAN +=
