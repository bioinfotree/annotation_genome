# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

extern ../phase_4/id.annot.gff3.gz as MAKER_GFF3


log:
	mkdir -p $@

tmp:
	mkdir -p $@

ALN = $(addsuffix .tophat2.bam,$(SAMPLES))
%.tophat2.bam:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_1/$(SAMPLE)/accepted_hits_RG.bam $(SAMPLE).tophat2.bam) \
	) \
	sleep 3


id.annot.gff3:
	zcat <$(MAKER_GFF3) >$@

query.fasta:
	ln -sf $(QUERY) $@


SEP :=,

# cuffdiff -p 8 \
# --library-type fr-firststrand \
# -o /projects/novabreed/lanes/alignments/tophat/rna/vitis_vinifera/genome_12xCHR/cuffdiff/rkatsiteli/GFF \
# -b /genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta \
# -u \
# -L berries,leaves,tendrils \
# /projects/vitis/genes/annotation/V2.1/V2.1.gff3 \
# ${BDIR}/berries/rk-b1/accepted_hits.bam,${BDIR}/berries/rk-b2/accepted_hits.bam \
# ${BDIR}/leaves/rk-l1/accepted_hits.bam,${BDIR}/leaves/rk-l2/accepted_hits.bam \
# ${BDIR}/tendrils/rk-t1/accepted_hits.bam,${BDIR}/tendrils/rk-t2/accepted_hits.bam
genes.fpkm_tracking: id.annot.gff3 query.fasta $(ALN)
	module purge; \
	module load lang/python/2.7.3; \
	ln -sf $$PRJ_ROOT/local/share/rules/cuffdiff.ptp .; \
	pipeline_mgmt launch flow cuffdiff.ptp \
	--label "$@" \
	--templates \
	annotation=$< \
	labels_string="$(LABELS_STRING)" \
	library_type=$(call get,$(call first,$(SAMPLES)),ST) \   * take from first sample *
	outdir=$$PWD \
	reference=$^2 \
	samples_string="$(SAMPLES_STRING)"


.PHONY: test
test:
	@echo 

ALL += genes.fpkm_tracking

INTERMEDIATE += 

CLEAN += id.annot.gff3 query.fasta
